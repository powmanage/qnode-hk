第一步：制作磁盘：
  gdisk /dev/nvme0n1
  gdisk /dev/nvme1n1
  ```
  Type `n` to create a new partition.
  Choose a partition number (usually 1 for the first EFI partition).
  Set the first sector to the default value.
  Set the last sector to specify the size (e.g., +16G for a 512MB EFI partition).
  Set the partition type to EFI System (code EF00).
  Save changes with `w`.
  ```
  mkfs.fat -F 32 -n QUBIC /dev/nvme0n1p1
  mkfs.fat -F 32 -n QUBIC /dev/nvme1n1p1

  mount /dev/nvme0n1p1 /mnt
  mount /dev/nvme1n1p1 /mnt

第二步：更改qubic库，由于nvme限制，需要修改:
  #define READING_CHUNK_SIZE 65536
  #define WRITING_CHUNK_SIZE 65536

第三步：
  修改BIOS，使SMT为DISABLE状态
  修改#define MAX_NUMBER_OF_PROCESSORS 15

第四步：
  修改地址： ADDRESS-1 保留为操作地址
  其他地址私钥填写在seed中
  所有自己的节点全部填在publicip中
  

第五步：
  测试连接是否正常：
   telnet 193.84.3.6 21841
  ./qubic-cli -nodeip 193.84.3.6  -getcurrenttick


UEFI :
step1: device -b, find all macs for other network
step2: ifconfig -l check
step3: 

startup.nsh
```
timezone -s 00:00
disconnect 20B
disconnect 20C
disconnect 215
disconnect 25A
disconnect 25B
disconnect 272
ifconfig -r
ifconfig -s eth1 static 193.84.3.6 255.255.255.0 193.84.3.1
ifconfig -s eth1 dns 8.8.8.8 8.8.4.4
fs0:
cd efi
cd boot
```

 mount /dev/nvme1n1p1 /mnt
 cd qnode-hk
 git pull
 rm -rf /mnt/*
 cp -r file/91.zip /mnt
 cp -r xx/efi /mnt
 cp -r Q-efi-file/xx/Qubic* /mnt/efi/boot/Qubic.efi
 cd /mnt
 unzip 91.zip
 rm -rf 91.zip
 tree
 cat efi/boot/startup.nsh
 


